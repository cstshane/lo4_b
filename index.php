<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Learning Outcome 4 - Connecting to a database</title>
    </head>
    <body>
        <h1>Learning Outcome 4 - Connecting to a database</h1>
        <ol>
            <li><a href="4-Procedural.php">
                The procedural approach
                </a></li>
            <li><a href="4-Object.php">
                The Object-oriented approach
                </a></li>
            <li><a href="4-OurObject.php">
                The Object-oriented approach with our own object
                </a></li>
        </ol>
    </body>
</html>
