<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>4-OurObject</title>
    </head>
    <body>
        <h1>4-OurObject</h1>

<?php
// require_once __DIR__ . '/../../private/CSTClasses_B/DbObject.php';
require_once __DIR__ . '/Autoloader.php';

$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );

use CSTClasses_B\DbObject;

//Open a connection to MySQL (call mysqli_connect)
$db = new DbObject();
echo "<p>Successfully connected to the database!</p>\n";
?>
    </body>
</html>
